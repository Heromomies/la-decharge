﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class Crafting : MonoBehaviour
{
    public Inventory Container;
    public InventoryObject inventory;
    public InventorySlot[] GetSlots
    {
        get { return Container.Slots; }
    }

    [System.NonSerialized] public UserInterface parent;
    public Item item = new Item();

    public ItemObject itemCraft
    {
        get
        {
            if (item.Id >= 0)
            {
                return parent.inventory.database.ItemObjects[item.Id];
            }

            return null;
        }
    }

    public InventorySlot FindItemOnInventory(Item _item) // Permet de savoir quel item est sur quel slot
    {
        for (int i = 0; i < GetSlots.Length; i++)
        {
            Debug.Log(_item.Id);
            if (GetSlots[i].item.Id == _item.Id)
            {
                return GetSlots[i];
            }
        }

        return null;
    }

    [System.Serializable]
    public class Inventory // Définir le nombre de slot que l'inventaire contient
    {
        public InventorySlot[] Slots = new InventorySlot[3];

        public void Clear() // Nettoie un slot quand on enlève un item
        {
            for (int i = 0; i < Slots.Length; i++)
            {
                Slots[i].RemoveItem();
            }
        }
    }

    public bool AddItem(Item _item, int _amount) // permet de savoir si je peux ajouter un item à un inventaire ou pas 
    {
        if (EmptySlotCount <= 0)
            return false;
        InventorySlot slot = FindItemOnInventory(_item);
        slot.AddAmount(_amount);
        return true;
    } 
    public int EmptySlotCount // Compte combien d'items sont dans l'inventaire
    {
        get
        {
            int counter = 0;
            for (int i = 0; i < GetSlots.Length; i++)
            {
                if (GetSlots[i].item.Id <= -1)
                {
                    counter++;
                }
            }
            return counter;
        }
    }
}
    
