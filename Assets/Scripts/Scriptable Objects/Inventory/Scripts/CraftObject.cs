﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEditor;
using System.Runtime.Serialization;

[CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory System/Inventory")]
public class CraftObject : ScriptableObject
{
    public ItemDatabaseObject database;
    public InterfaceType type;
    public Inventory1 Container;
    public InventorySlot1[] GetSlots1 { get { return Container.Slots; } }


    public bool AddItem(Item _item, int _amount)
    {
        if (EmptySlotCount <= 0)
            return false;
        InventorySlot1 slot = FindItemOnInventory(_item);
        if(!database.ItemObjects[_item.Id].stackable || slot == null)
        {
            SetEmptySlot(_item, _amount);
            return true;
        }
        slot.AddAmount(_amount);
        return true;
    }
    public int EmptySlotCount
    {
        get
        {
            int counter = 0;
            for (int i = 0; i < GetSlots1.Length; i++)
            {
                if (GetSlots1[i].item.Id <= -1)
                {
                    counter++;
                }
            }
            return counter;
        }
    }
    public InventorySlot1 FindItemOnInventory(Item _item)
    {
        for (int i = 0; i < GetSlots1.Length; i++)
        {
            if(GetSlots1[i].item.Id == _item.Id)
            {
                return GetSlots1[i];
            }
        }
        return null;
    }
    public InventorySlot1 SetEmptySlot(Item _item, int _amount)
    {
        for (int i = 0; i < GetSlots1.Length; i++)
        {
            if (GetSlots1[i].item.Id <= -1)
            {
                GetSlots1[i].UpdateSlot(_item, _amount);
                return GetSlots1[i];
            }
        }
        //set up functionality for full inventory
        return null;
    }

    public void SwapItems(InventorySlot1 item1, InventorySlot1 item2)
    { 
        InventorySlot1 temp = new InventorySlot1( item2.item, item2.amount);
            item2.UpdateSlot(item1.item, item1.amount);
            item1.UpdateSlot(temp.item, temp.amount);
    }
    [ContextMenu("Save")]
    [ContextMenu("Clear")]
    public void Clear()
    {
        Container.Clear();
    }
}
[System.Serializable]
public class Inventory1
{
    public InventorySlot1[] Slots = new InventorySlot1[30];
    public void Clear()
    {
        for (int i = 0; i < Slots.Length; i++)
        {
            Slots[i].RemoveItem();
        }
    }
}

public delegate void SlotUpdated1(InventorySlot1 _slot);

[System.Serializable]
public class InventorySlot1
{
    public ItemType[] AllowedItems = new ItemType[0];
    [System.NonSerialized]
    public UserInterface parent;
    [System.NonSerialized]
    public GameObject slotDisplay;
    [System.NonSerialized]
    public SlotUpdated1 OnAfterUpdate;
    [System.NonSerialized]
    public SlotUpdated1 OnBeforeUpdate;
    public Item item = new Item();
    public int amount;

    public ItemObject ItemObject
    {
        get
        {
            if(item.Id >= 0)
            {
                Debug.Log(ItemObject.data.Id);    
                return parent.inventory.database.ItemObjects[item.Id];
            }
            return null;
        }
    }

    public InventorySlot1()
    {
        UpdateSlot(new Item(), 0);
    }
    public InventorySlot1(Item _item, int _amount)
    {
        UpdateSlot(_item, _amount);
    }
    public void UpdateSlot(Item _item, int _amount)
    {
        if (OnBeforeUpdate != null)
            OnBeforeUpdate.Invoke(this);
        item = _item;
        amount = _amount;
        if (OnAfterUpdate != null)
            OnAfterUpdate.Invoke(this);
    }
    public void RemoveItem()
    {
        UpdateSlot(new Item(), 0);
    }
    public void AddAmount(int value)
    {
        UpdateSlot(item, amount += value);
    }
    public bool CanPlaceInSlot(ItemObject _itemObject)
    {
        if (AllowedItems.Length <= 0 || _itemObject == null || _itemObject.data.Id < 0)
            return true; Debug.Log(_itemObject.data.Id);
        for (int i = 0; i < AllowedItems.Length; i++)
        {
            if (_itemObject.type == AllowedItems[i])
                return true;
        }
        return false;
    }
}
