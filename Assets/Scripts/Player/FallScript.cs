﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FallScript : MonoBehaviour
{
    public TextMeshProUGUI textFall;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Vide"))
        {
            transform.position = Vector3.zero;
            textFall.text = " Quelle idée de tomber dans le vide ! ";
            StartCoroutine(TextFall());
        }
    }

    IEnumerator TextFall()
    {
        yield return new WaitForSeconds(2);
        textFall.text = "";
    }
}
