﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class TPSCameraController : MonoBehaviour
{
    public float rotationSpeed;

    public Transform player, target;

    private float mouseX, mouseY, joystickX, joystickY;
    public float minY, maxY;

    public bool cameraCanMove = true;

    void Start()
    {
        //Le curseur devient invisible et est bloqué au milieu de l'écran
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void LateUpdate()
    {
        CameraControl();    //On lance toutes les frames la fonction CameraControl
    }

    public void CameraControl()
    {
        //On attribue aux float mouseX et mouseY les Input.Axis de la sourit (récupère si on tourne la souris)
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;

        //Ici, on encadre la valeur Mouse Y, pour qu'on ne puisse pas tourné à 360°
        mouseY = Mathf.Clamp(mouseY, minY, maxY);

        //La caméra suit Target (la visière du player)
        transform.LookAt(target);

        if (cameraCanMove)
        {
           //On fait tourner la caméra selon les mouvements de la souris
            target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
            player.rotation = Quaternion.Euler(0, mouseX, 0);    //Si on reste appuyé sur le clic droit, on tourne la caméra et le player 
        }
        
    }
}
