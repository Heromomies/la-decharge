﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteraction : MonoBehaviour
{
    public float rayDistance;
    public LayerMask layer;

    public Image eKey;
    public TextMeshProUGUI feedbackFouille;
    
    private float cooldown;
    private bool _dejaAffiche;

    void Update()
    {
        //On envoie un Raycast en face du player
        RaycastHit hit;
        
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, rayDistance,
            layer))
        {
            if (!hit.transform.gameObject.GetComponent<NoMoreInteractable>().searched)
            {
                eKey.gameObject.SetActive(true);
            }
            else
            {
                eKey.gameObject.SetActive(false);
            }
            
            if (Input.GetKeyDown(KeyCode.E))
            {
                //i on appuie sur E et que l'objet visé possède le script NoMoreInteractable avec le boolean searched sur false, on met le boolean _launchCooldown du script fouille de l'objet visé sur true. Sinon, c'est que l'objet a déjà été fouillé
                if (!hit.transform.gameObject.GetComponent<NoMoreInteractable>().searched)
                {
                    hit.transform.gameObject.GetComponent<Fouille>()._launchCooldown = true;
                }
                else
                {
                    _dejaAffiche = true;
                }
            }

            if (_dejaAffiche)
            {
                cooldown += Time.deltaTime;
                feedbackFouille.text = "Déjà fouillé";
                
                if (cooldown >= 1f)
                {
                    feedbackFouille.text = "";
                }
                if (cooldown >= 2f)
                {
                    _dejaAffiche = false;
                    cooldown = 0;
                }
            }
        }
        else
        {
            eKey.gameObject.SetActive(false);
            feedbackFouille.text = "";
        }
        
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * rayDistance, Color.green);
    }
}
