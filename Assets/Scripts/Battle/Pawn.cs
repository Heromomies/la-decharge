﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public abstract class Pawn : MonoBehaviour
{
    public bool enemy;
    public Case currentCase;
    public int pv;
    private MeshRenderer _mr;
    //[HideInInspector]
    public float lastTurn;
    //[HideInInspector]
    public float nextTurn;
    //[HideInInspector]
    public bool started;
    void Awake()
    {
        _mr = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        started = false;
    }

    private void Update()
    {
        //Joue le tour et définie le prochain tour
        if (started && Time.time >= nextTurn)
        {
            OnTurn();
            lastTurn = nextTurn;
            nextTurn = nextTurn + BattleManager.Instance.turnTime;
        }
    }

    //Déplace l'élément à la nouvelle position
    public Tween Move(Case to)
    {    
        if (currentCase != null)
        {
            currentCase.onIt = null;
        }

        to.onIt = this;
        currentCase = to;
        return transform.DOMove(to.transform.position +
                                new Vector3(0,
                                    _mr.bounds.extents.y + to.transform.GetComponent<MeshRenderer>().bounds.extents.y,
                                    0), 0.3f);
    }

    //Appelé au début du tour
    public abstract void OnTurn();

    public void Damage(int dmg)
    {
        pv-= dmg;
        if (pv <= 0)
        {
            BattleManager.Instance.turnOrder.Remove(this);
            Destroy(gameObject);
        }
    }
}
