﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RemainingActions : MonoBehaviour
{
    public IntVariable playerActions;
    
    void Update()
    {
        GetComponent<TMP_Text>().text = playerActions.value.ToString();
    }
}
