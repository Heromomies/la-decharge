﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class EnemyPawn : Pawn
{
    public int move;
    public int atk;
    public int pvMax;
    public TMP_Text number;

    private void Start()
    {
        pv = pvMax;
    }

    public override void OnTurn()
    {
        //Déplace l'ennemi et attaque si un joueur est proche
        var anim = DOTween.Sequence();
        anim.Append(Move(BattleManager.Instance.ShortestWay(move, currentCase, BattleManager.Instance.player.currentCase)));
        if(BattleManager.Instance.CasesInRange(1, currentCase, false).Contains(BattleManager.Instance.player.currentCase))
        {
            anim.Append(transform.DOPunchScale(new Vector3(1,1,1),0.3f,1)).OnComplete(()=>BattleManager.Instance.player.Damage(atk));
            anim.Join(Camera.main.DOShakePosition(0.3f,0.1f));
        }
    }
}
