﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;
using Debug = UnityEngine.Debug;

public class Case : MonoBehaviour
{
    public Pawn onIt;
    public bool moveDisplay;
    public bool selected;
    public bool movePlanned;
    public bool attackPlanned;
    public int x;
    public int y;
    private MeshRenderer _mr;

    private void Start()
    {
        _mr = GetComponent<MeshRenderer>();
    }

    private void OnMouseEnter()
    {
        if (moveDisplay)
        {
            selected = true;
        }
    }

    private void OnMouseExit()
    {
        if (moveDisplay)
        {
            selected = false;
        }
    }

    //Met à jour la couleur de la case selon sa situation
    private void Update()
    {
        if (selected)
        {
            _mr.material.color = Color.green;
        }
        else if (attackPlanned)
        {
            _mr.material.color = Color.red;
        }
        else if (movePlanned)
        {
            _mr.material.color = Color.blue;
        }
        else if (moveDisplay)
        {
            _mr.material.color = Color.cyan;
        }
        else
        {
            _mr.material.color = Color.white;
        }
    }
}