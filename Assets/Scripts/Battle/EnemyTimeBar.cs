﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyTimeBar : MonoBehaviour
{
    public EnemyPawn enemy;
    public int yPos;

    void Update()
    {
        GetComponent<Slider>().maxValue = enemy.nextTurn - enemy.lastTurn;
        GetComponent<Slider>().value = Time.time - enemy.lastTurn;
        if (Camera.main != null)
            transform.position = Camera.main.WorldToScreenPoint(enemy.transform.position) + new Vector3(0, yPos, 0);
    }
}
