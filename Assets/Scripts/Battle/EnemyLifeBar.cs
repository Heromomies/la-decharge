﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLifeBar : MonoBehaviour
{
    public EnemyPawn enemy;
    public int yPos;

    private void Start()
    {
        GetComponent<Slider>().maxValue = enemy.pvMax;
    }

    void Update()
    {
        GetComponent<Slider>().value = enemy.pv;
        if (Camera.main != null)
            transform.position = Camera.main.WorldToScreenPoint(enemy.transform.position) + new Vector3(0, yPos, 0);
    }
}
