﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyEncounter : MonoBehaviour
{
    public List<GameObject> loot = new List<GameObject>();
    private void OnCollisionEnter(Collision other)
    {
        //Déclenche un combat lors d'une collision avec le joueur
        if (other.gameObject.CompareTag("Player") && !FindObjectOfType<InventorPanel>().inBattle)
        {
            BattleManager.Instance.StartBattle(loot[Random.Range(0,loot.Count)], transform.position);
            Destroy(gameObject);
        }
    }
}
