﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBarPlayer : MonoBehaviour
{
    public IntVariable playerLife;

    private void Start()
    {
        GetComponent<Slider>().maxValue = playerLife.maxValue;
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Slider>().value = playerLife.value;
    }
}
