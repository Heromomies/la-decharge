﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public List<GameObject> enemyList, saveEnemyList;
    public GameObject mobList;
    private int numDay;
    private bool actualiser;

    public BoolVariable day, night;
    
    void Start()
    {
        foreach (Transform child in transform)
        {
            enemyList.Add(child.gameObject);
        }

        saveEnemyList = enemyList;
        numDay = GetComponent<DayAndNightControl>().currentDay;
    }
    
    void Update()
    {
        /*if (numDay < GetComponent<DayAndNightControl>().currentDay)
        {
            enemyList = saveEnemyList;
            numDay = GetComponent<DayAndNightControl>().currentDay;
        }*/
        
        if (day.value)
        {
            actualiser = true;
            for (int i = 0; i < 5; i++)
            {
                enemyList[i].SetActive(false);
            }
        }

        if (night.value && actualiser)
        {
            enemyList = saveEnemyList;
            for (int i = 0; i < enemyList.Count; i++)
            {
                enemyList[i].SetActive(true);
            }

            actualiser = false;
        }
    }
}
