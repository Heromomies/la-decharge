﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using DG.Tweening;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class PlayerPawn : Pawn
{
    public IntVariable playerLife;
    private List<Case> _movable;
    public IntVariable actions;
    [HideInInspector]
    public Case moveTarget;
    private List<PlannedAction> _plannedActions;

    private void Start()
    {
        pv = playerLife.maxValue;
        _movable = new List<Case>();
    }

    //Affiche les cases utilisables à une certaine portée
    public void ShowMovable(int range)
    {
        _movable = BattleManager.Instance.CasesInRange(range, moveTarget, false);
        foreach (var tile in _movable)
        {
            tile.moveDisplay = true;
        }
    }

    //Efface les cases utilisables
    public void RemoveMovable()
    {
        foreach (var tile in _movable)
        {
            tile.moveDisplay = false;
            tile.selected = false;
        }
        _movable.Clear();
    }

    //Programme l'action du joueur
    public void AddAction(Item item, Case target)
    {
        if (actions.value > 0)
        {
            switch (item.action)
            {
                case Action.Attack:
                    if (target != moveTarget)
                    {
                        _plannedActions.Add(new PlannedAction(Action.Attack, item.range,target));
                        target.attackPlanned = true;
                        actions.value--;
                    }
                    break;
                case Action.Move:
                    if (target != moveTarget)
                    {
                        _plannedActions.Add(new PlannedAction(Action.Move, item.range, target));
                        target.movePlanned = true;
                        moveTarget = target;
                        actions.value--;
                    }
                    break;
            }
        }
    }

    public void FirstTurn()
    {
        moveTarget = currentCase;
        _plannedActions = new List<PlannedAction>();
        actions.value = actions.maxValue;
    }

    public override void OnTurn()
    {
        //Exécute les actions programmées dans l'ordre
        var anim = DOTween.Sequence();
        foreach (var action in _plannedActions)
        {
            switch (action.action)
            {
                case Action.Attack:
                    if (action.target.onIt != null)
                    {
                        anim.Append(transform.DOPunchScale(new Vector3(1,1,1),0.3f,1).OnComplete(() => {action.target.onIt.Damage(2);
                            BattleManager.Instance.CheckBattleEnd();}));
                    }
                    break;
                case Action.Move:
                    anim.Append(Move(BattleManager.Instance.ShortestWay(action.range, currentCase, action.target)));
                    break;
            }

            action.target.attackPlanned = false;
            action.target.movePlanned = false;
        }
        RemoveMovable();
        FirstTurn();
    }

    //Efface les actions programmés
    public void CancelActions()
    {
        foreach (var action in _plannedActions)
        {
            action.target.attackPlanned = false;
            action.target.movePlanned = false;
        }
        _plannedActions.Clear();
        moveTarget = currentCase;
        actions.value = actions.maxValue;
    }

    //Inflige des dégâts au joueur
    public new void Damage(int dmg)
    {
        if (playerLife.value - dmg <= 0)
        {
            playerLife.value = 0;
            BattleManager.Instance.EndBattle(false);
        }
        else
        {
            playerLife.value -= dmg;
        }

        pv = playerLife.value;
    }
    
    private class PlannedAction
    {
        public Action action;
        public int range;
        public Case target;

        public PlannedAction(Action action, int range, Case target)
        {
            this.action = action;
            this.range = range;
            this.target = target;
        }
    }
}
