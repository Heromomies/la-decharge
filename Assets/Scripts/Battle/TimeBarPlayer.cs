﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeBarPlayer : MonoBehaviour
{
    void Update()
    {
        if (FindObjectOfType<PlayerPawn>() != null)
        {
            var player = FindObjectOfType<PlayerPawn>();
            GetComponent<Slider>().maxValue = player.nextTurn - player.lastTurn;
            GetComponent<Slider>().value = Time.time - player.lastTurn;
        }
    }
}
