﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class BattleManager : MonoBehaviour
{
    public static BattleManager Instance;
    [Header("Grid parameters")]
    public float caseSize; // Taille d'une case
    public LayerMask gridLayer; //Layer des cases

    [Header("Player parameters")]
    public IntVariable playerLife; //Variable de la vie du joueur
    public GameObject playerObject; //gameobject du joueur hors combat
    public int secondsBetweenTurns; //Temps entre chaque tour

    [Header("Prefab and static object")]
    public GameObject caseObject; // prefab case
    public GameObject gridObject; // gameobject nécéssaire pour placer la grille
    public GameObject draggedItem; // prefab item glissé
    public GameObject dragItem; // prefab pour la barre d'action
    public Transform actionPanel; // barre d'action
    public GameObject playerPrefab; // prefab du pion du joueur en combat
    public GameObject enemyPrefab; // prefab du pion des ennemis
    public GameObject battleHud; // interface de combat
    public ItemObject baseAttack; // action d'attaque de base
    public ItemObject baseMove; // action de mouvement de base
    public InventoryObject equip; // équipements du joueur
    public GameObject victoryPanel; // panneau de victoire
    public GameObject losePanel; // panneau de défaite
    
    [HideInInspector]
    public List<Pawn> turnOrder; //ordre de jeu
    private Case[][] _grid; // grille
    [HideInInspector]
    public PlayerPawn player; // pion du joueur en combat
    [HideInInspector]
    public int turnTime; //temps avant le prochain tour d'un pion
    private GameObject _loot; // objet à loot en cas de victoire
    private Vector3 _lootPos; // position du loot

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    //Débute un combat
    public void StartBattle(GameObject loot, Vector3 lootPos)
    {
        _loot = loot;
        _lootPos = lootPos;
        //On active le bool inbattle pour éviter les conflits d'interface
        FindObjectOfType<InventorPanel>().inBattle = true;
        gridObject.SetActive(true);
        //On désactive le joueur hors-combat
        playerObject.SetActive(false);
        //On remet la vie à 0
        playerLife.value = playerLife.maxValue;
        GenerateGrid(10);
        //On place la grille de combat au dessus du joueur
        gridObject.transform.position = playerObject.transform.position + new Vector3(0, 10, 0);
        //On ajoute les 2 actions de base à la barre d'action
        Instantiate(dragItem, actionPanel).GetComponent<DragItem>().SetItem(baseMove);
        Instantiate(dragItem, actionPanel).GetComponent<DragItem>().SetItem(baseAttack);
        //On ajoute les items équipés à la barre d'actions
        foreach (var item in equip.Container.Slots)
        {
            if (item.ItemObject != null)
            {
                if (item.item.action != Action.None)
                {
                    Instantiate(dragItem, actionPanel).GetComponent<DragItem>().SetItem(item.ItemObject);
                }
            }
        }
        turnOrder = new List<Pawn>();
        //On créé le joueur et on le place
        player = Instantiate(playerPrefab).GetComponent<PlayerPawn>();
        player.Move(_grid[Random.Range(0,10)][Random.Range(0,10)]);
        //On gère le temps avant son tour
        player.lastTurn = Time.time;
        player.nextTurn = Time.time + secondsBetweenTurns*2;
        player.started = true;
        turnOrder.Add(player);
        //On génère les ennemis
        int nbEnemy = Random.Range(1, 4);
        for (int i = 0; i < nbEnemy; i++)
        {
            var enemy = Instantiate(enemyPrefab).GetComponent<EnemyPawn>();
            enemy.number.text = (i+1).ToString();
            int x;
            int y;
            do
            {
                x = Random.Range(0, 10);
                y = Random.Range(0, 10);
            } while (_grid[x][y].onIt != null);
            enemy.Move(_grid[x][y]);
            enemy.lastTurn = Time.time;
            enemy.nextTurn = turnOrder[i].nextTurn + secondsBetweenTurns;
            enemy.started = true;
            turnOrder.Add(enemy);
        }
        turnTime = secondsBetweenTurns * turnOrder.Count;
        player.FirstTurn();
        //On active l'interface
        battleHud.SetActive(true);
    }

    //Annule les actions programmés du joueur
    public void CancelActions()
    {
        player.CancelActions();
    }

    //Créer une grille de size x size
    private void GenerateGrid(int size)
    {
        _grid = new Case[size][];
        for (int i = 0; i < size; i++)
        {
            _grid[i] = new Case[size];
            for (int j = 0; j < size; j++)
            {
                //Pour chaque case de la grille, on créé un objet case et on le place
                var gridCaseObject = Instantiate(caseObject, gridObject.transform);
                //On place les cases selon si le nombre de case est pair ou impair
                if (size % 2 == 0)
                {
                    gridCaseObject.transform.position =
                        new Vector3(caseSize * (i - size / 2), 0, caseSize * (j - size / 2));
                }
                else
                {
                    gridCaseObject.transform.position = new Vector3(caseSize * (i - 0.5f - size / 2), 0,
                        caseSize * (j - 0.5f - size / 2));
                }

                _grid[i][j] = gridCaseObject.GetComponent<Case>();
                _grid[i][j].x = i;
                _grid[i][j].y = j;
                //On nomme la case selon ses coordonnées (facilite le debug)
                gridCaseObject.name = i + "," + j;
            }
        }
    }

    //Retourne toutes les cases à une certaine portée en tenant compte ou non des collisions avec les autres éléments
    public List<Case> CasesInRange(int range, Case origin, bool collision)
    {
        List<Case> caseListe = new List<Case>();
        caseListe.Add(origin);
        for (int i = 0; i < range; i++)
        {
            var addedCase = new List<Case>();
            foreach (var tile in caseListe)
            {
                addedCase = addedCase.Union(AdjacentCases(tile, collision)).ToList();
            }
            caseListe = caseListe.Union(addedCase).ToList();
        }

        return caseListe;
    }

    //Retourne les cases adjacente en tenant compte ou non des collisons avec les autres éléments
    public List<Case> AdjacentCases(Case tile, bool collision)
    {
        List<Case> adjCase = new List<Case>();
        if (tile.x - 1 >= 0)
        {
            if (collision)
            {
                if (_grid[tile.x - 1][tile.y].onIt == null)
                {
                    adjCase.Add(_grid[tile.x-1][tile.y]);   
                }
            }
            else
            {
                adjCase.Add(_grid[tile.x-1][tile.y]);
            }
        }
        if (tile.y - 1 >= 0)
        {
            if (collision)
            {
                if (_grid[tile.x][tile.y-1].onIt == null)
                {
                    adjCase.Add(_grid[tile.x][tile.y-1]);  
                }
            }
            else
            {
                adjCase.Add(_grid[tile.x][tile.y-1]);
            }
        }
        if (tile.x + 1 < _grid.Length)
        {
            if (collision)
            {
                if (_grid[tile.x+1][tile.y].onIt == null)
                {
                    adjCase.Add(_grid[tile.x+1][tile.y]);  
                }
            }
            else
            {
                adjCase.Add(_grid[tile.x+1][tile.y]);
            }
        }
        if (tile.y + 1 < _grid[tile.x].Length)
        {
            if (collision)
            {
                if (_grid[tile.x][tile.y+1].onIt == null)
                {
                    adjCase.Add(_grid[tile.x][tile.y+1]);  
                }
            }
            else
            {
                adjCase.Add(_grid[tile.x][tile.y+1]);
            }
        }

        return adjCase;
    }

    //Retourne la case vide la plus proche de la case target en tenant compte de la portée de déplacement et des obstacles
    public Case ShortestWay(int range, Case origin, Case target)
    {
        List<Case> movable = CasesInRange(range, origin, true);
        float value = Vector3.Distance(target.transform.position, movable[0].transform.position);
        Case dest = movable[0];
        foreach (var tile in movable)
        {
            if (Vector3.Distance(target.transform.position, tile.transform.position) < value)
            {
                value = Vector3.Distance(target.transform.position, tile.transform.position);
                dest = tile;
            }
        }
        return dest;
    }

    //Vérifie si il reste des ennemis et met fin qu combat
    public void CheckBattleEnd()
    {
        foreach (var pawn in turnOrder)
        {
            if (pawn.enemy)
            {
                return;
            }
        }
        EndBattle(true);
    }

    //Efface tous les éléments du combat et affiche le panneau de victoire ou de défaite
    public void EndBattle(bool win)
    {
        DOTween.KillAll();
        foreach (var pawn in turnOrder)
        {
            Destroy(pawn.gameObject);
        }
        gridObject.transform.position = Vector3.zero;
        gridObject.SetActive(false);
        playerObject.SetActive(true);
        battleHud.SetActive(false);
        foreach (Transform child in actionPanel.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in gridObject.transform)
        {
            if (!child.CompareTag("MainCamera"))
            {
                Destroy(child.gameObject);
            }
        }
        turnOrder.Clear();
        if (win)
        {
            victoryPanel.SetActive(true);
        }
        else
        {
            losePanel.SetActive(true);
        }
    }

    //Enlève le panneau de victoire et loot l'objet
    public void WinBattle()
    {
        victoryPanel.SetActive(false);
        FindObjectOfType<InventorPanel>().inBattle = false;
        Instantiate(_loot, _lootPos, Quaternion.identity);
    }

    //Enlève le panneau de défaite
    public void LoseBattle()
    {
        losePanel.SetActive(false);
        FindObjectOfType<InventorPanel>().inBattle = false;
    }
}
