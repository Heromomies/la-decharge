﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private GameObject _draggedItem;
    private Item _item;

    public void SetItem(ItemObject item)
    {
        _item = item.data;
        GetComponent<Image>().sprite = item.uiDisplay;
    }
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        //Affiche les cases utilisables
        _draggedItem = Instantiate(BattleManager.Instance.draggedItem, transform);
        _draggedItem.GetComponent<Image>().sprite = GetComponent<Image>().sprite;
        BattleManager.Instance.player.ShowMovable(_item.range);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Déplace l'item selon les cases
        _draggedItem.transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        //Si l'item est laché sur une case
        if (Physics.Raycast(ray, out hit, 1000,BattleManager.Instance.gridLayer))
        {
            Case cs = hit.collider.gameObject.GetComponent<Case>();
            //Si cette case est utilisable
            if (cs.moveDisplay)
            {
                //On excécute l'action du joueur
                //TODO get param from scriptable object
                BattleManager.Instance.player.AddAction(_item, cs);
            }
            cs.selected = false;
        }
        //On renvoi l'item à sa position et on efface les cases utilisables
        BattleManager.Instance.player.RemoveMovable();
        Destroy(_draggedItem);
    }
}
