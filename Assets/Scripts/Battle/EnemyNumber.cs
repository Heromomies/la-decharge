﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyNumber : MonoBehaviour
{
    public GameObject enemy;
    
    void Update()
    {
        if (Camera.main != null)
            transform.position = Camera.main.WorldToScreenPoint(enemy.transform.position);
    }
}
