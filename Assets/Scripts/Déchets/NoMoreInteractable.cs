﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class NoMoreInteractable : MonoBehaviour
{
    public bool searched;

    private void Update()
    {
        if (searched)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = Color.gray;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
        }
    }
}
