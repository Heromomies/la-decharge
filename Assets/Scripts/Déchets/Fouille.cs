
 using System.Collections;
using System.Collections.Generic;
using System.Text;
 using TMPro;
 using UnityEngine;
 using UnityEngine.UI;

 public class Fouille : MonoBehaviour
{
    public float cooldown;
    private GameObject playerLoot;
    public List<GameObject> loot = new List<GameObject>();
    public Slider fouilleSlider;
    public TextMeshProUGUI feedbackFouille;
    [HideInInspector]
    public bool _launchCooldown;

    private TPSCharacterController tpsCharacterController;
    private TPSCameraController tpsCameraController;

    void Start()
    {
        playerLoot = GameObject.FindGameObjectWithTag("PlayerLoot");
        tpsCharacterController = FindObjectOfType<TPSCharacterController>();
        tpsCameraController = FindObjectOfType<TPSCameraController>();
    }
    
    void Update()
    {
        if (_launchCooldown)
        {
            cooldown += Time.deltaTime;    //Si le boolean _launchCooldown est en true, on fait s'écouler le cooldown
            fouilleSlider.gameObject.SetActive(true);
            fouilleSlider.value = cooldown;
            tpsCharacterController.speed = 0;
            tpsCameraController.cameraCanMove = false;
        }

        //Lorsqu'il atteint 0
        if (cooldown >= 1)
        {
            //Si l'objet qu'on fouille a son boolean searched en true, l'objet a déjà été fouillé, sinon, on lance la fonction Recherche. On passe le boolean _launchCooldown en false et on réinitialise le cooldown
            if (!GetComponent<NoMoreInteractable>().searched)
            {
                Recherche();
                feedbackFouille.text = "Fouille effectuée !";
            }
        }

        if (cooldown >= 1.5f)
        {
            feedbackFouille.text = "";
            _launchCooldown = false;
            cooldown = 0;
            fouilleSlider.value = 0;
            fouilleSlider.gameObject.SetActive(false);
            tpsCharacterController.speed = 12f;
            Debug.Log(tpsCharacterController.speed);
            tpsCameraController.cameraCanMove = true;
        }
    }
    
    //C'est la fonction qui lootera les items trouvés
    public void Recherche()
    {
        GetComponent<NoMoreInteractable>().searched = true;
        Instantiate(loot[Random.Range(0, loot.Count)], playerLoot.transform.position, playerLoot.transform.rotation);
    }
}