﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GroundItem : MonoBehaviour, ISerializationCallbackReceiver
{
    public ItemObject item;

    public void OnAfterDeserialize()
    {

    }

    public void OnBeforeSerialize() // Avec d'être sérialiser, prend la valeur du sprite et change le sprite
    {
#if UNITY_EDITOR
        GetComponentInChildren<SpriteRenderer>().sprite = item.uiDisplay;
        EditorUtility.SetDirty(GetComponentInChildren<SpriteRenderer>());
#endif
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Dechets"))
        {
            transform.position = GameObject.FindGameObjectWithTag("PlayerLootBehind").transform.position;
        }
    }
}
