﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StaticInterface : UserInterface
{
    public GameObject[] slots;
    
    public GameObject result1, result2, result3, result4, result5;
    public Image case1, case2;
    public Sprite saveCase;
    public GameObject player;

    
    public override void CreateSlots() // Créer les slots et met à jour les Event Trigger
    {
        slotsOnInterface = new Dictionary<GameObject, InventorySlot>();
        for (int i = 0; i < inventory.GetSlots.Length; i++)
        {
            var obj = slots[i];
            
            AddEvent(obj, EventTriggerType.PointerEnter, delegate { OnEnter(obj); });
            AddEvent(obj, EventTriggerType.PointerExit, delegate { OnExit(obj); });
            AddEvent(obj, EventTriggerType.BeginDrag, delegate { OnDragStart(obj); });
            AddEvent(obj, EventTriggerType.EndDrag, delegate { OnDragEnd(obj); });
            AddEvent(obj, EventTriggerType.Drag, delegate { OnDrag(obj); });
            inventory.GetSlots[i].slotDisplay = obj;
            slotsOnInterface.Add(obj, inventory.GetSlots[i]);
        }
    }
    
    public void Confirmer()
    {
        for (int i = 0; i < inventory.GetSlots.Length; i++)
        {
            Debug.Log(inventory.GetSlots[i].item.Id, slots[i]);
        }

        if (inventory.GetSlots[0].item.Id == 3 && inventory.GetSlots[1].item.Id == 3)
        {
            Instantiate(result1, player.transform.position, Quaternion.identity);
            inventory.GetSlots[0].item.Id = -1;
            inventory.GetSlots[1].item.Id = -1;
            case1.sprite = saveCase;
            case2.sprite = saveCase;
        }
        
        if (inventory.GetSlots[0].item.Id == 1 && inventory.GetSlots[1].item.Id == 1)
        {
            Instantiate(result2, player.transform.position, Quaternion.identity);
            inventory.GetSlots[0].item.Id = -1;
            inventory.GetSlots[1].item.Id = -1;
            case1.sprite = saveCase;
            case2.sprite = saveCase;
        }
        
        if ((inventory.GetSlots[0].item.Id == 0 && inventory.GetSlots[1].item.Id == 8) || (inventory.GetSlots[0].item.Id == 8 && inventory.GetSlots[1].item.Id == 0))
        {
            result3 = Instantiate(result3, player.transform.position, Quaternion.identity);
            inventory.GetSlots[0].item.Id = -1;
            inventory.GetSlots[1].item.Id = -1;
            case1.sprite = saveCase;
            case2.sprite = saveCase;
        }
        
        if (inventory.GetSlots[0].item.Id == 10 && inventory.GetSlots[1].item.Id == 10)
        {
            result4 = Instantiate(result4, player.transform.position, Quaternion.identity);
            inventory.GetSlots[0].item.Id = -1;
            inventory.GetSlots[1].item.Id = -1;
            case1.sprite = saveCase;
            case2.sprite = saveCase;
        }
        
        if ((inventory.GetSlots[0].item.Id == 14 && inventory.GetSlots[1].item.Id == 19) || (inventory.GetSlots[0].item.Id == 19 && inventory.GetSlots[1].item.Id == 14))
        {
            result3 = Instantiate(result5, player.transform.position, Quaternion.identity);
            inventory.GetSlots[0].item.Id = -1;
            inventory.GetSlots[1].item.Id = -1;
            case1.sprite = saveCase;
            case2.sprite = saveCase;
        }
    }
}
