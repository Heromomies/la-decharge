﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInventory : MonoBehaviour
{
    public InventoryObject inventory;
    public InventoryObject equipment;
    private ItemObject item;
    public TextMeshProUGUI feedbackInventaire;
    
    public Attribute[] attributes;

    private void Start() //Permet d'update l'inventaire dès le début du start
    {
        for (int i = 0; i < attributes.Length; i++)
        {
            attributes[i].SetParent(this);
        }
        for (int i = 0; i < equipment.GetSlots.Length; i++)
        {
            equipment.GetSlots[i].OnBeforeUpdate += OnBeforeSlotUpdate;
            equipment.GetSlots[i].OnAfterUpdate += OnAfterSlotUpdate;
        }
    }


    public void OnBeforeSlotUpdate(InventorySlot _slot) // Si on déplace un item dans l'inventaire d'équipement
    {
        if (_slot.ItemObject == null)
            return;
        switch (_slot.parent.inventory.type)
        {
            case InterfaceType.Inventory:
                break;
            case InterfaceType.Equipment:
                print(string.Concat("Removed ", _slot.ItemObject, " on ", _slot.parent.inventory.type, ", Allowed Items: ", string.Join(", ", _slot.AllowedItems)));

                for (int i = 0; i < _slot.item.buffs.Length; i++)
                {
                    for (int j = 0; j < attributes.Length; j++)
                    {
                        if (attributes[j].type == _slot.item.buffs[i].attribute)
                            attributes[j].value.RemoveModifier(_slot.item.buffs[i]);
                    }
                }

                break;
            case InterfaceType.Chest:
                break;
            default:
                break;
        }
    }
    public void OnAfterSlotUpdate(InventorySlot _slot) // Après avoir placé un item dans l'équipement 
    {
        if (_slot.ItemObject == null)
            return;
        switch (_slot.parent.inventory.type)
        {
            case InterfaceType.Inventory:
                break;
            case InterfaceType.Equipment: // Si l'objet est placé dans l'équipement il regarde s'il est autorisé à être dedans
                print(string.Concat("Placed ", _slot.ItemObject, " on ", _slot.parent.inventory.type, ", Allowed Items: ", string.Join(", ", _slot.AllowedItems)));

                for (int i = 0; i < _slot.item.buffs.Length; i++)
                {
                    for (int j = 0; j < attributes.Length; j++)
                    {
                        if (attributes[j].type == _slot.item.buffs[i].attribute)
                            attributes[j].value.AddModifier(_slot.item.buffs[i]);
                    }
                }

                break;
            case InterfaceType.Chest:
                break;
            default:
                break;
        }
    }
    
    public void OnTriggerEnter(Collider other) // Quand le player entre en collision avec un item
    {
        var groundItem = other.GetComponent<GroundItem>();
        if (groundItem)
        {
            Item _item = new Item(groundItem.item);
            if (inventory.AddItem(_item, 1)) 
            {
                feedbackInventaire.text = "Objet récupéré !";
                StartCoroutine(WaitForText());
                Destroy(other.gameObject);
            }
            EncyclopediaManager.Instance.AddObjectsToEncyclopedia(item);
        }
    }

    IEnumerator WaitForText()
    {
        yield return new WaitForSeconds(1);
        feedbackInventaire.text = "";
    }
    public void Update() // Pour sauvegarder
    {
        /*if (Input.GetKeyDown(KeyCode.V))
        {
            inventory.Save();
            equipment.Save();
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            inventory.Load();
            equipment.Load();
        }*/
    }

    public void AttributeModified(Attribute attribute)  // Debug log la valeur de l'attribut
    {
        Debug.Log(string.Concat(attribute.type, " was updated! Value is now ", attribute.value.ModifiedValue));
    }
    
    private void OnApplicationQuit() // Quand on quitte l'application ça nettoie l'inventaire et l'équipement
    {
        inventory.Clear();
        equipment.Clear();
    }
}

[System.Serializable]
public class Attribute
{
    [System.NonSerialized]
    public PlayerInventory parent;
    public Attributes type;
    public ModifiableInt value;
    
    public void SetParent(PlayerInventory _parent) // Modifie la valeur des attributs des personnages
    {
        parent = _parent;
        value = new ModifiableInt(AttributeModified);
    }
    public void AttributeModified() // change la valeur du parent 
    {
        parent.AttributeModified(this);
    }
}