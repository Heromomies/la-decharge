﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private void LateUpdate() // Permet au objet de regarder vers la caméra
    {
        if (Camera.main != null) transform.forward = Camera.main.transform.forward;
    }
}
