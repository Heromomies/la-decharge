﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ResetBool : MonoBehaviour
{
    public bool test;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var items in GetComponent<EncyclopediaManager>().encyclopedia)
        {
            items.found = false;
        }
    }
}
