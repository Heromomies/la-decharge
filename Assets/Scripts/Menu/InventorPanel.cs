﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

public class InventorPanel : MonoBehaviour
{
    private bool _flash, _hasFlashed;

    public bool inventoryIsActive, isPaused, encyclopediaIsActive, craftIsActive, inBattle, recipesIsActive;
    public GameObject inventoryPanel, encyclopediaPanel, menuPause, craftPanel, recipes;

    private TPSCharacterController tpsCharacterController;
    private TPSCameraController tpsCameraController;

    private void Awake()
    {
        tpsCharacterController = FindObjectOfType<TPSCharacterController>();
        tpsCameraController = FindObjectOfType<TPSCameraController>();
    }

    void Update()
    {
        if (!_flash && !_hasFlashed)
        {
            inventoryPanel.SetActive(true);
            encyclopediaPanel.SetActive(true);
            menuPause.SetActive(true);
            _flash = true;
        }
        else if(_flash && !_hasFlashed)
        {
            inventoryPanel.SetActive(false);
            encyclopediaPanel.SetActive(false);
            menuPause.SetActive(false);
            _hasFlashed = true;
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            if (!inventoryIsActive && !inBattle && !encyclopediaIsActive && !isPaused && !recipesIsActive)
            {
                recipes.SetActive(true);
                recipesIsActive = true;
            }
            else
            {
                recipes.SetActive(false);
                recipesIsActive = false;
            }
        }
        
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (!inventoryIsActive && !inBattle && !encyclopediaIsActive && !isPaused && !recipesIsActive)
            {
                inventoryPanel.SetActive(true);
                inventoryIsActive = true;
            }
            else
            {
                inventoryPanel.SetActive(false);
                inventoryIsActive = false;
            }
        }
        
        if (Input.GetButtonDown("Encyclopedia"))
        {
            if (!encyclopediaIsActive && !inBattle && !inventoryIsActive && !isPaused && !recipesIsActive)
            {
                encyclopediaPanel.SetActive(true);
                encyclopediaIsActive = true;
            }
            else
            {
                encyclopediaPanel.SetActive(false);
                encyclopediaIsActive = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (!craftIsActive && !encyclopediaIsActive && !inBattle && !isPaused && !recipesIsActive)
            {
                craftPanel.SetActive(true);
                craftIsActive = true;
            }
            else
            {
                craftPanel.SetActive(false);
                craftIsActive = false;
            }
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                Debug.Log(isPaused);
                menuPause.SetActive(true);
                
                encyclopediaPanel.SetActive(false);
                encyclopediaIsActive = false;
                
                inventoryPanel.SetActive(false);
                inventoryIsActive = false;
                
                craftPanel.SetActive(false);
                craftIsActive = false;
                
                recipes.SetActive(false);
                recipesIsActive = false;
                
                isPaused = true;
                Time.timeScale = 0f;
            }

            else
            {
                menuPause.SetActive(false);
                isPaused = false;
                Time.timeScale = 1f;
            }
        }
        


        if (inventoryIsActive || encyclopediaIsActive || craftIsActive || isPaused || inBattle || recipesIsActive)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            tpsCharacterController.speed = 0;
            tpsCameraController.cameraCanMove = false;
        }
        else if(!inventoryIsActive && !encyclopediaIsActive && !craftIsActive && !isPaused && !inBattle && !recipesIsActive)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked; 
            tpsCharacterController.speed = 12;
            tpsCameraController.cameraCanMove = true;
        }
    }
}
