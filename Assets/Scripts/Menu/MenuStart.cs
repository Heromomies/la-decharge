﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class MenuStart : MonoBehaviour
{
    public GameObject panelControls, panelInformations, panelPrinc;
    
    public void Play()
    {
        SceneManager.LoadScene("Proto");
        Time.timeScale = 1f;
    }

    public void Control()
    {
        panelControls.SetActive(true);
        panelInformations.SetActive(false);
        panelPrinc.SetActive(false);
    }

    public void QuitControl()
    {
        panelControls.SetActive(false);
        panelInformations.SetActive(false);
        panelPrinc.SetActive(true);
    }

    public void Information()
    {
        panelControls.SetActive(false);
        panelInformations.SetActive(true);
        panelPrinc.SetActive(false);
    }
    public void Quit()
    {
        Application.Quit();
    }
}
