﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour
{

    public GameObject menuPause;


    public void Reprendre()
    {
       FindObjectOfType<InventorPanel>().isPaused = false;
        Time.timeScale = 1f;
        menuPause.SetActive(false);
    }

    public void Menu()
    {
        SceneManager.LoadScene("MenuStart");
    }

    public void Quitter()
    {
        Application.Quit();
    }
}
