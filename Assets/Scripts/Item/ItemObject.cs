﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum ItemType // Type d'items disponibles
{
    Food,
    Helmet,
    Weapon,
    Shield,
    Boots,
    Chest,
    Default
}

public enum Attributes // type d'attributs 
{
    Agility,
    Intellect,
    Stamina,
    Strength
}

public enum Action // type d'action 
{
    None,
    Move,
    Attack
}

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory System/Items/item")]
public class ItemObject : ScriptableObject
{
    public Sprite uiDisplay;
    public bool stackable;
    public ItemType type;
    public bool found;
    [TextArea(15, 20)]
    public string description;
    public Item data = new Item();
    private ItemCollec itemCollec;
    public string objectName;

    public void FindObject()
    {
        itemCollec.SetImage(uiDisplay);
        itemCollec.SetText(objectName);
    }

    public void SetUi(ItemCollec ui)
    {
        itemCollec = ui;
    }
    public Item CreateItem() // Créer un nouvel item
    {
        Item newItem = new Item(this);
        return newItem;
    }
}

[System.Serializable]
public class Item 
{
    public string Name;
    public int Id = -1;
    public Action action;
    public int range;
    public ItemBuff[] buffs;
    public bool found;
    
    public Item() // Défini le nom de l'item et son ID
    {
        Name = "";
        Id = -1;
        action = Action.None;
        range = 0;
        found = false;
    }
    public Item(ItemObject item) // Prends le nom et l'ID de l'objet et met à jour les buffs et les attributs
    {
        Name = item.name;
        Id = item.data.Id;
        action = item.data.action;
        range = item.data.range;
        found = item.data.found;
        buffs = new ItemBuff[item.data.buffs.Length];
        for (int i = 0; i < buffs.Length; i++)
        {
            buffs[i] = new ItemBuff(item.data.buffs[i].min, item.data.buffs[i].max)
            {
                attribute = item.data.buffs[i].attribute
            };
        }
    }
}

[System.Serializable]
public class ItemBuff : IModifier
{
    public Attributes attribute;
    public int value;
    public int min;
    public int max;
    public ItemBuff(int _min, int _max) // Buff aléatoire
    {
        min = _min;
        max = _max;
        GenerateValue();
    }

    public void AddValue(ref int baseValue) // Change la valeur 
    {
        baseValue += value;
    }

    public void GenerateValue() // Génère une valeur aléatoire
    {
        value = UnityEngine.Random.Range(min, max);
    }
}