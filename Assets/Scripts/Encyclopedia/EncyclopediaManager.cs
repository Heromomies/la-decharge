﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class EncyclopediaManager : MonoBehaviour
{
    public static EncyclopediaManager Instance;
    public List<ItemObject> encyclopedia;
    public GameObject panel;
    public TextMeshProUGUI feedbackEncyclopedia;
    public int collectionFull, nmbintheCollection;
    private bool _encylopediaIsActive;
    public Rigidbody bullet;
    public Transform onPlayer;
    public bool canShoot, found;

    void Awake()
    {
        canShoot = true;
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }
    public void AddObjectsToEncyclopedia(ItemObject obj)
    {
        //si l'objet n'est pas trouvé, on le trouve et on l'affiche dans l'encyclopédie
        if (!obj.found)
        {
            feedbackEncyclopedia.text = "Un objet a été ajouté à votre collection !";
            StartCoroutine(WaitForText());
            Debug.Log("found");
            obj.found = true;
            obj.FindObject();
            collectionFull++;
        }
        else
        {
            feedbackEncyclopedia.text = "";
        }
    }
    
    public void Update()
    {
        if (collectionFull >= nmbintheCollection && canShoot == true)
        {
            InvokeRepeating("LaunchBullet", 2.0f, 0.3f);
            feedbackEncyclopedia.text = "Votre collection est terminée !";
        }
    }

    void LaunchBullet()
    {
        Rigidbody instance = Instantiate(bullet, onPlayer.transform.position, onPlayer.transform.rotation);
        instance.GetComponent<MeshRenderer>().material.color = new Color( Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)); 
        StartCoroutine(StopEnd());
    }
    IEnumerator WaitForText()
    {
        yield return new WaitForSeconds(3);
        feedbackEncyclopedia.text = "";
    }
    IEnumerator StopEnd()
    {
        yield return new WaitForSeconds(3);
        canShoot = false;
        feedbackEncyclopedia.text = "";
        CancelInvoke();
    }
    /*  public void OnApplicationQuit()
      {
          objects.found = false;
      }*/
}
    