﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Objects : ScriptableObject
{
  public string objectName;
  public Sprite img;
  public bool found;
  private ItemCollec itemCollec;

  //fonction qui sert a mettre le nom et l'image dans l'inv
  public void FindObject()
  {
    Debug.Log("affiche inv");
    itemCollec.SetImage(img);
    itemCollec.SetText(objectName);
  }

  public void SetUi(ItemCollec ui)
  {
    itemCollec = ui;
  }
}
