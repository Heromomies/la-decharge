﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class ItemCollec : MonoBehaviour
{
    public TMP_Text text;
    public Image img;
    public bool found;

    public void SetText(string txt)
    {
        text.text = txt;
        found = true;
    }
    public void SetImage(Sprite image)
    {
        img.sprite = image;
    }
}