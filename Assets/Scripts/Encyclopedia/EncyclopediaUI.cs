﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class EncyclopediaUI : MonoBehaviour
{
    public EncyclopediaManager encyclopediaManager;
    public ItemCollec itemCollec; 
   
    void Start()
    {
        
        foreach (var items in encyclopediaManager.encyclopedia)
        {
            items.SetUi(Instantiate(itemCollec, encyclopediaManager.panel.transform));
        }
    }
    public void Update()
    {
        foreach (var items in encyclopediaManager.encyclopedia)
        {
            if (items.found == true)
            {
                PlayerPrefs.SetInt("Found", 1);
            }
            else if(items.found == false)
            {
                PlayerPrefs.SetInt("Found", 0);
            }
        }
        if (Input.GetKey(KeyCode.V))
        {
           
        }
        if (Input.GetKey(KeyCode.N))
        {
            
        }
    }
    public static bool GetBool (string key)
    {  
        return PlayerPrefs.GetInt(key) == 1;
    }

    public static void SetBool (string key, bool state)
    {
        PlayerPrefs.SetInt (key, state ? 1 : 0);
    }
}  