using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute()]
	public class ES3Type_ScriptableObject : ES3ScriptableObjectType
	{
		public static ES3Type Instance = null;

		public ES3Type_ScriptableObject() : base(typeof(UnityEngine.ScriptableObject)){ Instance = this; }

		protected override void WriteScriptableObject(object obj, ES3Writer writer)
		{
			var instance = (UnityEngine.ScriptableObject)obj;
			
		}

		protected override void ReadScriptableObject<T>(ES3Reader reader, object obj)
		{
			var instance = (UnityEngine.ScriptableObject)obj;
			foreach(string propertyName in reader.Properties)
			{
				switch(propertyName)
				{
					
					default:
						reader.Skip();
						break;
				}
			}
		}
	}

	public class ES3Type_ScriptableObjectArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_ScriptableObjectArray() : base(typeof(UnityEngine.ScriptableObject[]), ES3Type_ScriptableObject.Instance)
		{
			Instance = this;
		}
	}
}